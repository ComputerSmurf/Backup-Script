#!/bin/bash

#what to backup.
backup_files="/srv /etc /var/lib/mysql "

#what to exclude (dont remove --exclude)
exclude_files="--exclude *.iso *.tgz *.tar.gz *.zip /Downloads"

# Denstination to backup.
dest="/backup"

# Create archive filename.

day=$(date +%d-%m-%Y)
hostname=$(hostname -s)
archive_file="$hostname-$day.tgz"

# show status.
echo "Backupt $backup_files to $dest/$archive_file"
date
echo

# Backup files using tar.
tar czf $dest/$archive_file $backup_files $exclude_files

# Show end status message.
echo
echo "Backup Finished"
date

# Long listing of files in $dest to check file sizes.
ls -lh $dest
